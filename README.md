Role Name
=========

Common role to set up a hosted network's servers.

Requirements
------------

Role Variables
--------------

Dependencies
------------

Python 3 is needed on the server. This role will do its best to install that.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: common }

License
-------

BSD

Author Information
------------------

smacz@fastmail.com
@smacz:matrix.org
